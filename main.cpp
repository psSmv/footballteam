#include <iostream>
#include <vector>
#include <iterator>

using std::cin;
using std::cout;
using std::endl;
using std::vector;

// Пара чисел, характеризующая игрока
struct Player {
    unsigned int efficiency;
    int id;
};

// Слияние двух отсортированных последовательностейв одну
template <
typename TInputIteratorOne,
typename TInputIteratorTwo,
typename TOutputIterator,
typename TComparator
>
void Merge (TInputIteratorOne beginOne,
            TInputIteratorOne endOne,
            TInputIteratorTwo beginTwo,
            TInputIteratorTwo endTwo,
            TOutputIterator output,
            TComparator compare)
{
    while (beginOne < endOne && beginTwo < endTwo) {
        if (compare(*beginOne, *beginTwo)) {
            *output = *beginOne;
            ++beginOne;
            ++output;
        } else {
            *output = *beginTwo;
            ++beginTwo;
            ++output;
        }
    }
    
    while (beginOne < endOne) {
        *output = *beginOne;
        ++output;
        ++beginOne;
    }
    
    while (beginTwo < endTwo) {
        *output = *beginTwo;
        ++output;
        ++beginTwo;
    }
};

// Сортировка слиянием со сравнением функцией compare
template <typename TComparator, typename TIteratorType>
void MergeSort (TIteratorType begin, TIteratorType end, TComparator compare) {
    
    size_t size = end - begin;
    
    TIteratorType middle = begin;
    std::advance(middle, size / 2);
    
    if (size > 1) {
        MergeSort(begin, middle, compare);
        MergeSort(middle, end, compare);
        
        vector<typename TIteratorType::value_type> buffer(size);
        Merge(begin, middle, middle, end, buffer.begin(), compare);
        
        for (size_t i = 0; i < size; ++i) {
            *begin = buffer[i];
            ++begin;
        }
    }
};

struct CmpEfficiencyGreater {
    bool operator () (const Player& first, const Player& second) const {
        return first.efficiency > second.efficiency;
    }
};

struct CmpIdLess {
    bool operator () (const Player& first, const Player& second) const {
        return first.id < second.id;
    }
};

// Поиск самой эффективной команды
vector<Player> findBestTeam(vector<Player> allPlayers) {
    
    MergeSort(allPlayers.begin(), allPlayers.end(), CmpEfficiencyGreater());
    
    if (allPlayers.size() <= 1) {
        vector<Player> bestTeam (allPlayers.begin(), allPlayers.end());
        return bestTeam;
    }
    
    int leftBorder = 0;
    int rightBorder = 1;
    int leftTeamBorder, rightTeamBorder;
    unsigned long long totalEfficiency =
    allPlayers[0].efficiency + allPlayers[1].efficiency;
    unsigned long long teamEfficiency = 0;
    
    while (leftBorder + 1 < allPlayers.size()) {
        
        while (allPlayers[leftBorder].efficiency <=
               allPlayers[rightBorder].efficiency + allPlayers[rightBorder + 1].efficiency
               && rightBorder + 1 < allPlayers.size()) {
            ++rightBorder;
            totalEfficiency += allPlayers[rightBorder].efficiency;
        }
        
        if (totalEfficiency > teamEfficiency) {
            teamEfficiency = totalEfficiency;
            leftTeamBorder = leftBorder;
            rightTeamBorder = rightBorder;
        }
        
        totalEfficiency -= allPlayers[leftBorder].efficiency;
        ++leftBorder;
    }
    
    ++rightTeamBorder;
    
    vector<Player> bestTeam(allPlayers.begin() + leftTeamBorder,
                            allPlayers.begin() + rightTeamBorder);
    return bestTeam;
};

// Ввод эффективностей игроков
vector<Player> readPlayers(std::istream& inputStream = cin) {
    int numberOfPlayers;
    vector<Player> allPlayers;
    inputStream >> numberOfPlayers;
    for (int i = 0; i < numberOfPlayers; ++i) {
        Player input;
        input.id = i + 1;
        inputStream >> input.efficiency;
        allPlayers.push_back(input);
    }
    return allPlayers;
};

// Вывод эффективности команды и номеров ее игроков
void printTeam (const vector<Player>& team, std::ostream& outputStream = std::cout) {
    unsigned long long teamEfficiency = 0;
    
    for (int i = 0; i < team.size(); ++i) {
        teamEfficiency += team[i].efficiency;
    }
    
    outputStream << teamEfficiency << endl;
    
    for (int i = 0; i < team.size(); ++i) {
        outputStream << team[i].id << ' ';
    }
    outputStream << endl;
};

int main()
{
    
    vector<Player> allPlayers = readPlayers();
    
    vector<Player> bestTeam = findBestTeam(allPlayers);
    
    MergeSort(bestTeam.begin(), bestTeam.end(), CmpIdLess());
    
    printTeam(bestTeam);
    
    return 0;
}
